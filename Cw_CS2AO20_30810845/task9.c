#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
    pid_t pid;

    // Fork a child process
    pid = fork();

    if (pid < 0) {
        // Fork failed
        fprintf(stderr, "Fork failed\n");
        return 1;
    } else if (pid == 0) {
        // Child process
        printf("Child process executing...\n");
        // Simulate some work
        sleep(3);
        printf("Child process completed\n");
        exit(0);
    } else {
        // Parent process
        printf("Parent process waiting for child...\n");
        int status;
        wait(&status); // Parent waits for the child to finish
        printf("Child process has completed\n");
        printf("Parent process exiting\n");
    }

    return 0;
}
