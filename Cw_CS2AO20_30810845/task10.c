#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>


#define N 3 /* define the total number of threads we want */
float total = 0; /* Set global variable */
pthread_mutex_t totalMutex = PTHREAD_MUTEX_INITIALIZER;
void* compute(void* arg) {
int pid = (int)pthread_self();
int i;
float result = 0;
for (i = 0; i < 2000000000; i++) {
result = sqrt(1000.0) * sqrt(1000.0);
}
printf("Result of %d is %f\n", pid, result);
pthread_mutex_lock(&totalMutex);
total += result;
printf("Total of %d is %f\n", pid, total);
pthread_mutex_unlock(&totalMutex);
pthread_exit(NULL);
}
int main() {
pthread_t tid[N];
int i;
for (i = 0; i < N; i++) {
if (pthread_create(&tid[i], NULL, compute, NULL) != 0) {
perror("Failed to create thread");
exit(1);
}
}
for (i = 0; i < N; i++) {
pthread_join(tid[i], NULL);
}
printf("Final total is %f\n", total);
return 0;
}

