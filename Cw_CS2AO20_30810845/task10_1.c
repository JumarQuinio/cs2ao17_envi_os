#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>

#define N 3 /* define the total number of threads we want */
float total = 0; /* Set global variable */

void *compute(void *arg) {
    int pid = *((int *)arg);
    int i;
    float result = 0;
    for (i = 0; i < 2000000000; i++) /* for a large number of times */
        result = sqrt(1000.0) * sqrt(1000.0);
    printf("Result of %d is %f\n", pid, result); /* Prints the result */
    total = total + result; /* to keep a running total in the global variable total */
    printf("Total of %d is %f\n", pid, total); /* Print running total so far. */
    pthread_exit(NULL);
}

int main() {
    pthread_t tid[N];
    int i, pid[N];

    for (i = 0; i < N; i++) { /* to loop and create the required number of threads */
        pid[i] = i;
        if (pthread_create(&tid[i], NULL, compute, &pid[i]) != 0) { /* Create thread and pass the process ID */
            perror("pthread_create");
            exit(1);
        }
    }

    for (i = 0; i < N; i++) { /* Wait for all threads to finish */
        if (pthread_join(tid[i], NULL) != 0) {
            perror("pthread_join");
            exit(1);
        }
    }

    return 0;
}
