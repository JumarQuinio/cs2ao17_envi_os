#include <stdio.h>
#include <unistd.h>

int main() {
    pid_t pid = getpid(); // Get the PID of the current process
    pid_t ppid = getppid(); // Get the PPID of the current process
    
    printf("PID: %d\n", pid);
    printf("PPID: %d\n", ppid);
    
    return 0;
}

